variable "s3_conf" {

}

variable "default_s3_conf" {
  default = {
    create_bucket                         = true
    attach_elb_log_delivery_policy        = false
    attach_lb_log_delivery_policy         = false
    attach_deny_insecure_transport_policy = false
    attach_require_latest_tls_policy      = false
    attach_policy                         = false
    attach_public_policy                  = true
    attach_inventory_destination_policy   = false
    attach_analytics_destination_policy   = false
    bucket                                = null
    bucket_prefix                         = null
    acl                                   = null
    policy                                = null
    tags                                  = {}
    force_destroy                         = false
    acceleration_status                   = null
    request_payer                         = null
    website                               = {}
    cors_rule                             = []
    versioning                            = {}
    logging                               = {}
    grant                                 = []
    owner                                 = {}
    expected_bucket_owner                 = null
    lifecycle_rule                        = []
    replication_configuration             = {}
    server_side_encryption_configuration  = {}
    intelligent_tiering                   = {}
    object_lock_configuration             = {}
    metric_configuration                  = []
    inventory_configuration               = {}
    inventory_source_account_id           = null
    inventory_source_bucket_arn           = null
    inventory_self_source_destination     = false
    analytics_configuration               = {}
    analytics_source_account_id           = null
    analytics_source_bucket_arn           = null
    analytics_self_source_destination     = false
    object_lock_enabled                   = false
    block_public_acls                     = false
    block_public_policy                   = false
    ignore_public_acls                    = false
    restrict_public_buckets               = false
    control_object_ownership              = false
    object_ownership                      = "ObjectWriter"
    putin_khuylo                          = true
  }
}
